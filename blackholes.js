// let canvas = document.querySelector('canvas');

// let ctx = canvas.getContext('2d');

// let width = canvas.width = window.innerWidth;
// let height = canvas.height = window.innerHeight
let idleInterval = setInterval(function () { stanceZero(zero, 300, -1100, 4075, -45), 150 }, 150)
let idleInterval2 = setInterval(function () { stanceZero(ken, 400, -3388, 3975, 0), 100, 150 }, 150)


function random(min, max) {
    let num = Math.floor(Math.random() * (max - min + 1)) + min;
    return num;
}

function Player(name, x, y, velX, velY, size, idleStart, id, healthElementId) {
    this.name = name
    this.x = x
    this.y = y
    this.velX = velX
    this.velY = velY
    this.size = size
    this.idleStart = idleStart
    this.element = document.getElementById(id)
    this.healthElement = document.getElementById(healthElementId)
    this.moveStart = 0
    this.hp = 100
}

function SuperPlayer(name, x, y, velX, velY, size, idleStart, id, healthElementId){
    Player.call(this, name, x, y, velX, velY, size, idleStart, id, healthElementId);
    this.hp = 140
}

SuperPlayer.prototype = Object.create(Player.prototype)
SuperPlayer.prototype.constructor = SuperPlayer


function Projectile(xStart, direction) {
    this.xStart = xStart
    this.direction = direction;
    this.velocity = 6;

    this.objectSprite = document.createElement("img")
    this.objectSprite.id = "Mumbo"
    this.objectSprite.src = "sor.png"
    this.objectSprite.display = "absolute"
    this.objectSprite.top = '1525px'
    this.objectSprite.left = this.xStart + 'px'

    const destination = document.querySelector('#Ken')

    destination.appendChild(this.objectSprite)
    setTimeout(function () {
        var element = document.getElementById("Mumbo")
        element.parentNode.removeChild(element);
    }, 2000);


    function SuperProjectile(xStart, direction) {
        Projectile.call(this, xStart, direction)
        this.objectSprite.style.size = "10px"

    }

    SuperProjectile.prototype = Object.create(Projectile.prototype)

    SuperProjectile.prototype.constructor = SuperProjectile;

}

Player.prototype.move = function (addAmount, moveChange, moveEnd, moveRestart, moveY) {
    this.x += addAmount * this.velX
    this.element.style.left = this.x + 'px'

    if (this.name == 'Zero') {
        if (addAmount == -1) this.element.style.transform = 'scaleX(-1)'
        else this.element.style.transform = 'scaleX(1)'
    } else {
        if (addAmount == 1) this.element.style.transform = 'scaleX(-1)'
        else this.element.style.transform = 'scaleX(1)'
    }

    console.log('hey')

    this.moveStart += moveChange;
    if (this.moveStart <= moveEnd) this.moveStart = moveRestart

    this.element.style.backgroundPosition = this.moveStart + `px ${moveY}px`
}
Player.prototype.kenAtk = function () {
    let newProjectile = new Projectile(this.x, this.direction)
    // let newerProjectile = new SuperProjectile(this.x, this.direction)
}
Player.prototype.zeroAtk = function () {
    this.element.style.backgroundImage = "url('r_Zero_Gif.gif')"
    this.element.style.backgroundPosition = '0px 0px';
    this.element.style.backgroundSize = 'cover'
    this.element.style.backgroundRepeat = 'no-repeat'
    console.log(this.y)
    let y = this.y
    let x = this.x
    console.log(this.y)
    this.element.style.top = (y - 600) + 'px'
    this.element.style.left = (x - 400) + 'px'
    this.element.style.width = '1000px'
    this.element.style.height = '1000px'
    // this.element.style = '-webkit-transform: scaleX(-1);'
    setTimeout(function () {
        let element = document.getElementById('Zero')
        element.style.backgroundImage = "url('zerox6.gif')"
        element.style.backgroundPosition = '-348px -4100px';
        element.style.backgroundSize = '6000px'
        element.style.width = '300px'
        element.style.height = '350px'
        element.style.backgroundRepeat = 'repeat'
        element.style.top = (y) + 'px'
        element.style.left = (x) + 'px'
        idleInterval = setInterval(function () { stanceZero(zero, 300, -1100, 4075, -45) }, 150)
    }, 4000)
    console.log(this.y)
}






Player.prototype.attackMove = function (enemy) {
    console.log(Math.abs(Number(this.x) - Number(enemy.x)))
    if (Math.abs(Number(this.x) - Number(enemy.x)) <= 500) {
        enemy.hp -= 10;
    }

    enemy.healthElement.style.width = enemy.hp + '%'
    console.log(this.name)
    if (this.name === 'Ken') this.kenAtk()
    else this.zeroAtk()
    if (enemy.hp == 0) {
        alert(`${this.name} wins`)
        location.reload()
    }
    
}

const zero = new SuperPlayer('Zero', 1800, 1690, 30, 10, 10, -45, 'Zero', 'zerohealth')
const ken = new Player('Ken', 1000, 1525, 30, 10, 10, 0, 'Ken', 'kenhealth')
// const projectile = new Projectile ()

const moveChoices = {
    // 'ArrowUp': () => zero.move(),
    'ArrowRight': () => {
        clearInterval(idleInterval)
        zero.move(+1, -340.5, -5107.5, -681, -4100)
        idleInterval = setInterval(function () { stanceZero(zero, 300, -1100, 4075, -45) }, 150)
    },
    // 'ArrowDown': () => zero.move(+1),
    'ArrowLeft': () => {
        clearInterval(idleInterval)
        zero.move(-1, -340.5, -5107.5, -681, -4100)
        idleInterval = setInterval(function () { stanceZero(zero, 300, -1100, 4075, -45) }, 150)
    },
    // 'w': () => ken.move(),
    'd': () => {
        clearInterval(idleInterval2)
        ken.move(+1, -690.5, -1900.5, -420, -500)
        idleInterval2 = setInterval(function () { stanceZero(ken, 400, -3388, 0, 0) }, 150)
    },
    // 's': () => ken.move(),
    'a': () => {
        clearInterval(idleInterval2)
        ken.move(-1, -690.5, -1900.5, -420, -500)
        idleInterval2 = setInterval(function () { stanceZero(ken, 400, -3388, 0, 0) }, 150)
    },
    'x': () => ken.attackMove(zero),

    'Shift': () => {
        clearInterval(idleInterval)
        zero.attackMove(ken)

        
    },
    'z': () => ken.attackMove(zero)




}



// let idleInterval = setInterval(stanceZero, 150)

function stanceZero(Character, xChange, xEnd, yStart, xStart) {
    if (Character.idleStart > xEnd) {
        Character.idleStart -= xChange
        Character.element.style.backgroundPosition = Character.idleStart + `px ${yStart}px`;
    } else if (Character.idleStart <= xEnd) {
        Character.idleStart = xStart
    }
}




document.addEventListener('keydown', (e) => {
    moveChoices[e.key]()
})

var battling = true;
var playerHp = 20;
var enemyHp = 100;

//Player's Attack Turn
var playerTurn = function () {
    var playerAccuracy = Math.floor(Math.random() * 3 + 1);
    var playerAttackDamage = Math.floor(Math.random() * 20 + 1);

    if (playerAccuracy > 1) {
        enemyHp -= playerAttackDamage;
        let div1 = document.createElement("p")
        div1.appendChild(document.createTextNode("Zero uses SlashStorm.  Ken suffers" + " " + playerAttackDamage + " " + "damage!\nZero:" + " " + playerHp + "HP\nKen:" + " " + enemyHp + "HP"));
        // document.getElementById("container").appendChild(div1).style.position = "fixed|relative|"
        document.getElementById("fightLog").appendChild(div1).style.size = 1 + "px"
        container.style.color = "red";
    } else {
        let div1 = document.createElement("p")
        div1.appendChild(document.createTextNode("Your attack missed!"));
        document.getElementById("fightLog").appendChild(div1)
    }
};


//Enemy's Attack Turn
var enemyTurn = function () {
    var enemyAccuracy = Math.floor(Math.random() * 5 + 1);
    var enemyAttackDamage = Math.floor(Math.random() * 5 + 1);

    if (enemyAccuracy > 3) {
        let div1 = document.createElement("p")
        playerHp -= enemyAttackDamage;
        div1.appendChild(document.createTextNode("Ken uses ShinkuHADOKEN!  You suffer" + " " + enemyAttackDamage + " " + "damage!\nZero:" + " " + playerHp + "HP\nKen:" + " " + enemyHp + "HP"));
        document.getElementById("fightLog").appendChild(div1)
        // container.div2.position = center;
    } else {
        let div1 = document.createElement("p")
        div1.appendChild(document.createTextNode("ken misses!"));
        document.getElementById("fightLog").appendChild(div1)
    }
};

var battle = function () {
    console.log("MentalBattle");
    let div1 = document.createElement("p")
    while (battling) {
        playerTurn();
        if (enemyHp <= 0) {
            div1.appendChild(document.createTextNode("Victory!"));
            document.getElementById("fightLog").appendChild(div1)
            battling = false;
        }
        if (enemyHp > 0) {
            enemyTurn();
            if (playerHp <= 0) {
                let div1 = document.createElement("p")
                div1.appendChild(document.createTextNode("Defeat!"));
                document.getElementById("fightLog").appendChild
                battling = false;
            }
        }
    }
};
battle();




